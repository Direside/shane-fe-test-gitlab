## Commit Zero

This app is created to work with Commit Zero. You'll need to run the `stack` application against a `config.yaml` file to generate the code.

### Stack Command

```bash
stack -config <config file> <source directory> <destination directory>
```

#### Config File

See `example.yml` for the full example.

```yaml
---
name: example-commit-zero-frontend

# params are key value pairs passed into templates
params:

  # Application config

  # production host name
  productionHost: fe-test.commitzero.com
  productionBucket: fe-test.commitzero.com

  # staging host name
  stagingHost: fe-test.commitzero.com
  stagingBucket: fe-test.commitzero.com

```

_Once you've templated this out, you can remove the above section from this README as it's no longer needed._

---

# shane-fe-test-gitlab

## Running locally

You'll need something to serve up the public directory on localhost. Since this project is framework-less, I'll leave that up to you.

## Building

As part of the build process we direct to one of the JSON files in the `config/` directory. You'll see this configured on line 1 of `main.js`. We use `sed` to do a string replace so if you don't have that installed you will need to do so for the builds to work.

### Staging

```zsh
make build-staging
```

### Production

```zsh
make build-production
```
