var configFile = "config/development.json"

function getConfig(callback) {
  fetch(`${configFile}`)
  .then(result => {
    return result.json()
  })
  .then(data => {
    callback(data)
  })
  .catch(() => {
    document.getElementById("error").setAttribute("visibility", "visible")
    document.getElementById("error-text").innerText = `Unable to load environment file (${configFile})`
  });
}

function getStatus(config) {
  document.getElementById("app-name").innerText = config.appName
  document.getElementById("environment").innerText = config.environment

  fetch(`${config.backendURL}/status/about`)
  .then(result => {
    document.getElementById("status-text").innerText = result.status
    return result.json()
  })
  .then(data => {
    document.getElementById("pod-name").innerText = data.podName
  })
  .catch(error => {
    document.getElementById("error").setAttribute("visibility", "visible")
    document.getElementById("error-text").innerText = error
  });
}

getConfig(getStatus);